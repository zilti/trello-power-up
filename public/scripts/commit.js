/* global popup:false */
/* global api:false */
/* global utils:false */
/* global project:false */
/* global Promise:false */

'use strict';
var Commit = function Commit() {
};

Commit.prototype.attachCommit = function attachCommit(t, url, name) {
  return t.attach({
    url: url,
    name: name
  })
    .then(popup.close.bind(this, t))
    .catch(function onErroro(error) {
      throw t.NotHandled('error occurred while attaching commit to card', error);
    });
};

Commit.prototype.checkAuth = function getAuth(authStatus) {
  return new Promise(function callback(resolve, reject) {
    if (authStatus && authStatus.authorized) {
      resolve();
    } else {
      reject();
    }
  });
};

Commit.prototype.attach = function attach(t) {
  return utils.getAuthorizationStatus(t)
    .then(this.checkAuth)
    .then(function onSuccess() {
      return project.showProjects(t, this.showProjectCommits.bind(this));
    }.bind(this), function onError() {
      return popup.openConfigure(t);
    });
};

Commit.prototype.getCommits = function getCommits(t, project) {
  return utils.getAuthToken(t)
    .then(api.getRecentCommits.bind(api, project.id))
    .catch(function onError(error) {
      throw t.NotHandled('error occurred while getting commits from GitLab API', error);
    });
};

Commit.prototype.showProjectsCallback = function showProjectsCallback(t, project, response) {
  var self = this;
  return response.data.map(function map(commit) {
    var text = commit.short_id + ' ' + commit.title;
    return {
      text: text,
      callback: function callbackWrapper(trelloInstance) {
        return self.attachCommit(trelloInstance, project.web_url + '/commit/' + commit.short_id, text + ' · ' + project.name_with_namespace);
      }
    };
  });
};

Commit.prototype.showProjectCommits = function showProjectCommit(t, project) {
  return this.getCommits(t, project)
    .then(this.showProjectsCallback.bind(this, t, project))
    .then(popup.openCommits.bind(this, t))
    .catch(function onError(error) {
      throw t.NotHandled('error occurred while contacting GitLab API', error);
    });
};

window.commit = new Commit();
