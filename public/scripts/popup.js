/* global mergeRequest:false */
/* global commit:false */

function openConfigure(t) {
  return t.popup({
    title: 'Configure',
    url: './configure.html',
    height: 470
  });
}

function openAuthorize(t) {
  return t.popup({
    title: 'Authorize',
    url: 'authorize.html',
    height: 195
  });
}

function openAttachIssue(t) {
  return t.popup({
    title: 'Attach Issue (Coming Soon)',
    url: './issue.html',
    height: 210
  });
}

function openMergeRequests(t, itemsToSearch) {
  return t.popup({
    title: 'Merge Requests',
    items: itemsToSearch,
    search: {
      count: 10,
      placeholder: 'Search merge requests',
      empty: 'No merge requests found'
    }
  });
}

function openCommits(t, commits) {
  return t.popup({
    title: 'Recent Commits',
    items: commits,
    search: {
      count: 10,
      placeholder: 'Search commits',
      empty: 'No recent commits found'
    }
  });
}

function openProjects(t, itemsToSearch) {
  return t.popup({
    title: 'Projects',
    items: itemsToSearch,
    search: {
      count: 10,
      placeholder: 'Search Projects',
      empty: 'No projects found'
    }
  });
}

function openCard(t) {
  return t.popup({
    title: 'GitLab',
    items: [
      {
        text: 'Attach Merge Request',
        callback: mergeRequest.attach
      }, {
        text: 'Attach Issue (Coming Soon)',
        callback: openAttachIssue
      },
      {
        text: 'Attach Commit',
        callback: commit.attach.bind(commit)
      }
    ]
  });
}

function close(t) {
  return t.closePopup();
}

window.popup = {
  openConfigure: openConfigure,
  openAuthorize: openAuthorize,
  openProjects: openProjects,
  openMergeRequests: openMergeRequests,
  openCard: openCard,
  close: close,
  openCommits: openCommits
};
