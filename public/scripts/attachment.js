/* global utils:false */

var t = TrelloPowerUp.iframe();

t.render(function render() {
  t.card('attachments')
    .get('attachments')
    .filter(utils.isGitLabAttachment)
    .then(function appendAttachments(gitlabAttachments) {
      document.getElementById('content').innerHTML = '';

      gitlabAttachments.forEach(function appendAttachment(attachment) {
        var linkType = utils.getAttachmentType(attachment);
        var a = document.createElement('a');
        a.classList.add('gl-link');
        a.classList.add(linkType);
        a.classList.add('quiet-button');
        a.setAttribute('href', attachment.url);
        a.setAttribute('target', '_blank');
        a.setAttribute('rel', 'noopener noreferrer');
        a.innerText = attachment.name;

        document.getElementById('content').appendChild(a);
      });
    })
    .then(utils.sizeContainer);
});
