/* global mergeRequest:false */
/* global utils:false */
/* global api:false */
/* global popup:false */

function getProjectsError(t, error) {
  throw t.NotHandled('error occurred when getting projects from GitLab API', error);
}

function getProjects(t) {
  return utils.getAuthToken(t)
    .then(api.getAllProjects)
    .catch(getProjectsError.bind(this, t));
}

function mapProjects(fnSelectProjectCallback, p) {
  return {
    text: p.name_with_namespace,
    callback: function selectProject(t) {
      return fnSelectProjectCallback(t, p);
    }
  };
}

function showProjectsCallback(fnSelectProjectCallback, response) {
  return response.data.map(mapProjects.bind(this, fnSelectProjectCallback));
}

function showProjectsError(t, error) {
  throw t.NotHandled('error occurred while displaying projects', error);
}

function showProjects(t, fnSelectProjectCallback) {
  return getProjects(t)
    .then(showProjectsCallback.bind(this, fnSelectProjectCallback))
    .then(popup.openProjects.bind(this, t))
    .catch(showProjectsError.bind(this, t));
}

window.project = {
  showProjects: showProjects
};
